let params = new URLSearchParams(window.location.search)
let courseId = params.get("courseId")
let token = localStorage.getItem("token")

/*
Activity
Create a DELETE request via fetch that sends the courseId in the URL, then if the server responds
with a true statement (meaning disabling the course worked), immediately redirect the user to courses.html
else, display an error message.
*/
if(!token){
	alert("You need to login first to update the course")
	window.location.replcace("./login.html")
}else{
	fetch(`http://localhost:4000/courses/${courseId}`,{
		method: "DELETE",
		headers:{
			Authorization: `Bearer ${token}`,
		}
	})
	.then(res=> res.json())
	.then(data => {
		if(data===true){
			window.location.replace("./courses.html")
		}else{
			aleart("Deleting course failed. Please try again.")
		}
	})
}


