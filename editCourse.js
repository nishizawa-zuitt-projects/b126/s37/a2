let params = new URLSearchParams(window.location.search)

let courseId = params.get("courseId")
let courseName = document.querySelector("#courseName")
let courseDesc = document.querySelector("#courseDescription")
let coursePrice = document.querySelector("#coursePrice")
let token = localStorage.getItem("token")


fetch(`http://localhost:4000/courses/${courseId}`)
.then(res=>res.json())
.then(data=> {
	courseName.value = data.name
	courseDesc.value = data.description
	coursePrice.value = data.price

	document.querySelector("#editCourse").addEventListener("submit", (e) =>{
		e.preventDefault()

		// Variables representing the values fo our inputs
		let name = courseName.value
		let desc = courseDesc.value
		let price = coursePrice.value
/*
Activities:
Create a PUT request via fetch that will send our new, updated course information to the server
and handle the server's response. If the server responds with true (use===true), show an alert that
confirms successful course updating, then redirect the user to courses.html
*/

		if(!token){
			alert("You need to login first to update the course")
			window.location.replcace("./login.html")
		}else{
			fetch(`http://localhost:4000/courses/${courseId}`,{
				method: "PUT",
				headers:{
					Authorization: `Bearer ${token}`,
					'Content-Type':'application/json'
				},
				body:  JSON.stringify({
				name:name,
				description:desc,
				price: price
				})
			})
			.then(res => res.json())
			.then(data => {
				if(data === true){
					alert("The course has been updated successfully.")
					window.location.replace("./courses.html")
				}else{
					alert("Updating a course was unsuccess. Please try again.")
					window.location.replace("./courses.html")
				}
			})
		}
	})
})












